# Kanban

### Inicialização

clone o repositório em sua máquina:

```bash
    git clone git@gitlab.com:emanuel-jose/kanban.git
```

Para iniciar o backend, siga as instruções [aqui](https://gitlab.com/emanuel-jose/kanban/-/tree/master/BACK)

Para iniciar o front entre na pasta FRONT e instale as dependências:

```bash
    cd FRONT/
    # rode esse comando para instalar as dependências do pacote package.json:
    
    yarn

    # rode o comando para iniciar o servidor e abrir a aplicação

    yarn start

```

**Atenção é necessário que o backend tenha sido inicializado para poder funcionar corretamente as features da aplicação**


## Projeto

O deafio era construir um kanban em que fosse possivel adicionar cards as colunas e poder alternar entre elas, além de editar o card e persistir os dados usando a API na pasta BACK.

<img src='./images/home.png' alt='home'>

Ao clicar no botão com a seta para baixo irá abrir um formulário simples para adicionar um card a coluna TODO

<img src='./images/open-form.png' alt='open-form'>

O campo de texto também suporta markdown

<img src='./images/add-card.png' alt='add-card'>

Ao clicar no card irá abrir as informações

<img src='./images/view-card.png'>
<img src='./images/card.png'>

É possivel editar ou excluir o card e também mudar a coluna com as setas direcionais

<img src='./images/move.png' alt='move-card'>

### Tarefas futuras:
 - Backend em Typescript
 - Animações e Drag in Drop
 - Highlights Markdown