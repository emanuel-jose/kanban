import React, { useEffect, useState } from 'react'
import { MainContainer, ColumnsContainer } from './style'
import FormCreateCard from '../../components/FormCreateCard'
import Column from '../../components/Column'
import axios from 'axios'

import { CardSchema } from '../../interfaces/card'

const Home: React.FC = () => {
  const [cards, setCards] = useState<CardSchema[]>([])

  const getToken = () => {
    const data = {
      login: 'letscode',
      senha: 'lets@123'
    }

    axios
      .post('http://0.0.0.0:5000/login/', data, {
        headers: {
          Accept: 'text/html',
          ContentType: 'application/json'
        }
      })
      .then((res) => localStorage.setItem('token', res.data))
      .catch((err) => console.log(err))
  }

  const getCards = () => {
    axios
      .get('http://0.0.0.0:5000/cards/', {
        headers: {
          Accept: 'text/html',
          ContentType: 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then((res) => setCards(res.data))
      .catch((err) => console.log(err))
  }

  useEffect(getToken, [])
  useEffect(getCards, [])

  return (
    <MainContainer>
      <FormCreateCard setCards={setCards} />
      <ColumnsContainer>
        <Column name="TODO" cards={cards} setCards={setCards} />
        <Column name="DOING" cards={cards} setCards={setCards} />
        <Column name="DONE" cards={cards} setCards={setCards} />
      </ColumnsContainer>
    </MainContainer>
  )
}

export default Home
