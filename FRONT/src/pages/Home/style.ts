import styled from 'styled-components'

export const MainContainer = styled.div`
  width: 100vw;

  margin: 0 auto;

  display: flex;
  flex-direction: column;
  align-items: center;

  @media (min-width: 720px) {
    width: max-content;

    flex-direction: row;
    align-items: flex-start;
  }
`

export const ColumnsContainer = styled.div`
  @media (min-width: 720px) {
    display: flex;
    align-items: center;

    min-width: max-content;
  }
`
