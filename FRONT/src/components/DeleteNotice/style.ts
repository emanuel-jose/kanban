import styled from 'styled-components'

export const Container = styled.div`
  background-color: var(--bg-card-color);
  width: 75vw;
  height: 15rem;
  border-radius: 5px;

  position: absolute;

  h1 {
    text-align: center;
    color: var(--card-text-color);
    text-transform: uppercase;
    font-weight: 500;
    margin: 3rem 0 2rem 0;
  }

  button {
    border: none;

    width: 10rem;
    height: 5rem;

    border-radius: 2rem;

    font-size: 1.6rem;
    color: var(--card-text-color);

    transition: background 0.3s;
  }

  button.cancel {
    background-color: var(--bg-color);
  }

  button.cancel:hover {
    background-color: var(--bg-button-color);
    cursor: pointer;
  }

  button.delete {
    background-color: #9066f4;
  }

  button.delete:hover {
    background-color: #875af4;
    cursor: pointer;
  }

  @media (min-width: 720px) {
    width: 50rem;
    height: 23rem;
  }
`
export const ButtonContainer = styled.div`
  width: 100%;

  display: flex;
  justify-content: space-around;

  @media (min-width: 720px) {
    height: 15rem;
    align-items: center;
  }
`
