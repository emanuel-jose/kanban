import React from 'react'
import { Container, ButtonContainer } from './style'

import { CardSchema } from '../../interfaces/card'
import axios from 'axios'

interface DeleteProps extends Pick<CardSchema, 'id'> {
  setCards: React.Dispatch<React.SetStateAction<CardSchema[]>>
  setOpenModal: React.Dispatch<React.SetStateAction<boolean>>
  setDeleteNotice: React.Dispatch<React.SetStateAction<boolean>>
}

const DeleteNotice: React.FC<DeleteProps> = ({
  id,
  setCards,
  setOpenModal,
  setDeleteNotice
}) => {
  const cancel = (): void => {
    setDeleteNotice(false)
  }

  const deleteCard = (): void => {
    axios
      .delete(`http://0.0.0.0:5000/cards/${id}`, {
        headers: {
          Accept: 'text/html',
          ContentType: 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then((res) => setCards(res.data))

    setOpenModal(false)
  }
  return (
    <Container>
      <h1>Confirm Deletion ?</h1>
      <ButtonContainer>
        <button className="cancel" onClick={cancel}>
          NO
        </button>
        <button className="delete" onClick={deleteCard}>
          OK
        </button>
      </ButtonContainer>
    </Container>
  )
}

export default DeleteNotice
