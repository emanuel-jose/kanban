import React, { ButtonHTMLAttributes } from 'react'
import { IconButton } from './style'

interface IconButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  icon: JSX.Element
  size?: number
}

const IconButtonComponent: React.FC<IconButtonProps> = ({
  icon,
  size,
  ...rest
}) => {
  return (
    <IconButton {...rest}>
      <span style={{ fontSize: `${size}px` }}>{icon}</span>
    </IconButton>
  )
}

export default IconButtonComponent
