import styled from 'styled-components'

export const IconButton = styled.button`
  border: none;
  background-color: var(--bg-card-color);

  :hover {
    cursor: pointer;
  }

  span {
    color: var(--card-text-color);
    font-size: 1.8rem;
  }
`
