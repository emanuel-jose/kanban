import React, { FormHTMLAttributes } from 'react'
import { Form } from './style'

export interface FormProps extends FormHTMLAttributes<HTMLFormElement> {
  open?: boolean
}

const FormComponent: React.FC<FormProps> = ({ children, open, ...rest }) => {
  return (
    <Form style={{ height: open ? '40rem' : '8rem' }} {...rest}>
      {children}
    </Form>
  )
}

export default FormComponent
