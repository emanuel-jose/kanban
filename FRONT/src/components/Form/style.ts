import styled from 'styled-components'

export const Form = styled.form`
  background-color: var(--bg-form);
  width: 80vw;
  border-radius: 5px;

  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;

  @media (min-width: 720px) {
    width: 30rem;

    margin: 1rem;

    padding: 0.5rem;

    input {
      width: 25rem;
    }
  }
`
