import styled from 'styled-components'

export const TextArea = styled.textarea`
  resize: none;
  font-size: 1.6rem;
  border: none;
  border-radius: 5px;

  width: 25rem;
  height: 20rem;

  padding-left: 1rem;
  padding-top: 1rem;
`
