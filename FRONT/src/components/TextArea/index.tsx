import React, { TextareaHTMLAttributes } from 'react'
import { TextArea } from './style'

type TextAreaProps = TextareaHTMLAttributes<HTMLTextAreaElement>

const TextAreaComponent: React.FC<TextAreaProps> = ({ ...rest }) => {
  return <TextArea {...rest} />
}

export default TextAreaComponent
