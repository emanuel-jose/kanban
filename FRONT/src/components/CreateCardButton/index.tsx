import React, { ButtonHTMLAttributes } from 'react'
import { Button, IconContainer, Icon } from './style'

type ButtonProps = ButtonHTMLAttributes<HTMLButtonElement>

interface Props extends ButtonProps {
  icons: JSX.Element
}

const CreateCardButton: React.FC<Props> = ({ icons, ...rest }) => {
  return (
    <Button {...rest}>
      <IconContainer>
        <Icon>{icons}</Icon>
      </IconContainer>
    </Button>
  )
}

export default CreateCardButton
