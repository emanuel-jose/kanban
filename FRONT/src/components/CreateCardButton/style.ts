import styled from 'styled-components'

export const Button = styled.button`
  width: 80vw;
  height: 8rem;
  background-color: var(--bg-button-color);
  border: none;
  border-radius: 5px;
  transition: background 0.3s;

  display: flex;
  align-items: center;
  justify-content: center;

  :hover {
    cursor: pointer;
    background-color: var(--bg-btn-hover);
  }

  @media (min-width: 720px) {
    width: 30rem;
  }
`

export const IconContainer = styled.div`
  background-color: var(--bg-card-icons-color);
  width: 40%;
  height: 5rem;
  border-radius: 5px;

  display: flex;
  justify-content: center;
  align-items: center;
`
export const Icon = styled.span`
  font-size: 3rem;
  color: var(--icons-button-color);
  size: 20px;
`
