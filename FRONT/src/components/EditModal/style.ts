import styled from 'styled-components'

export const Container = styled.div`
  background-color: var(--bg-card-color);
  width: 90vw;
  height: 35rem;
  border-radius: 5px;

  display: flex;
  flex-direction: column;
  align-items: center;

  @media (min-width: 720px) {
    width: 55rem;
    height: 45rem;

    .inputEdit {
      width: 50rem;
    }

    .textEdit {
      width: 50rem;
      height: 25rem;
    }
  }
`

export const ButtonsContainer = styled.div`
  width: 80%;
  display: flex;
  justify-content: space-between;

  @media (min-width: 720px) {
    height: 10rem;
  }
`
