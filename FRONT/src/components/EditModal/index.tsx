import React, { useState } from 'react'
import Input from '../Input'
import TextArea from '../TextArea'
import IconButton from '../IconButton'
import { TiCancel } from 'react-icons/ti'
import { VscSaveAs } from 'react-icons/vsc'
import { Container, ButtonsContainer } from './style'

import { CardSchema } from '../../interfaces/card'
import axios from 'axios'

interface EditProps extends Pick<CardSchema, 'id' | 'titulo' | 'conteudo'> {
  cards: CardSchema[]
  setCards: React.Dispatch<React.SetStateAction<CardSchema[]>>
  setOpenEdit: React.Dispatch<React.SetStateAction<boolean>>
}

type Fields = Pick<EditProps, 'titulo' | 'conteudo'>

const EditModal: React.FC<EditProps> = ({
  id,
  titulo,
  conteudo,
  cards,
  setCards,
  setOpenEdit
}) => {
  const [fields, setFields] = useState<Fields>({
    titulo,
    conteudo
  })

  const handleTitle = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const { conteudo } = fields

    setFields({
      titulo: e.target.value,
      conteudo
    })
  }

  const handleContent = (e: React.ChangeEvent<HTMLTextAreaElement>): void => {
    const { titulo } = fields

    setFields({
      titulo,
      conteudo: e.target.value
    })
  }

  const populateCards = () => {
    axios
      .get('http://0.0.0.0:5000/cards/', {
        headers: {
          Accept: 'text/html',
          ContentType: 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then((res) => setCards(res.data))
      .catch((err) => console.log(err))
  }

  const editCardDB = (card: CardSchema) => {
    axios
      .put(`http://0.0.0.0:5000/cards/${id}`, card, {
        headers: {
          Accept: 'text/html',
          ContentType: 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(() => populateCards())
      .catch((err) => console.log(err))
  }

  const handleSaveEdit = () => {
    const newCards = cards.map((card) => card)

    const indexCard = newCards.findIndex((card) => card.id === id)

    const editedCard = newCards[indexCard]

    editedCard.titulo = fields.titulo ? fields.titulo : titulo
    editedCard.conteudo = fields.conteudo ? fields.conteudo : conteudo

    editCardDB(editedCard)
    setOpenEdit(false)
  }

  return (
    <Container>
      <Input
        className="inputEdit"
        placeholder={titulo}
        onChange={handleTitle}
        value={fields.titulo}
      />
      <TextArea
        className="textEdit"
        placeholder="Description..."
        onChange={handleContent}
        value={fields.conteudo}
      />
      <ButtonsContainer>
        <IconButton
          icon={<TiCancel />}
          size={40}
          onClick={() => setOpenEdit(false)}
          title="cancel"
        />
        <IconButton
          icon={<VscSaveAs />}
          size={35}
          onClick={handleSaveEdit}
          title="save"
        />
      </ButtonsContainer>
    </Container>
  )
}

export default EditModal
