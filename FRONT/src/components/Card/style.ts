import styled from 'styled-components'

export const CardContainer = styled.div`
  background-color: var(--bg-card-color);
  width: 25rem;
  height: 7rem;
  border-radius: 5px;

  display: flex;
  justify-content: center;
  align-items: center;

  margin: 0.5rem 0;

  transition: background 0.3s;

  :hover {
    background-color: var(--card-hover);
    cursor: pointer;
  }

  h1 {
    color: var(--card-text-color);
    font-weight: normal;
  }
`
