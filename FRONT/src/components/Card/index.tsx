import React, { useState } from 'react'
import CardModal from '../CardModal'
import { CardContainer } from './style'

import { CardSchema } from '../../interfaces/card'

interface CardProps extends CardSchema {
  cards: CardSchema[]
  setCards: React.Dispatch<React.SetStateAction<CardSchema[]>>
}

const Card: React.FC<CardProps> = ({
  id,
  titulo,
  conteudo,
  lista,
  cards,
  setCards
}) => {
  const [openModal, setOpenModal] = useState<boolean>(false)

  return (
    <>
      <CardContainer
        onClick={() => {
          setOpenModal(true)
        }}
      >
        <h1>{titulo}</h1>
      </CardContainer>

      {openModal && (
        <CardModal
          id={id}
          titulo={titulo}
          conteudo={conteudo}
          lista={lista}
          setOpenModal={setOpenModal}
          setCards={setCards}
          cards={cards}
        />
      )}
    </>
  )
}

export default Card
