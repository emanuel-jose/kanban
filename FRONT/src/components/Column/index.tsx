import React from 'react'
import Card from '../Card'
import { ColumnContainer, CardsContainer } from './style'

import { CardSchema } from '../../interfaces/card'

interface ColumnProps {
  name: 'TODO' | 'DOING' | 'DONE'
  cards: CardSchema[]
  setCards: React.Dispatch<React.SetStateAction<CardSchema[]>>
}

const Column: React.FC<ColumnProps> = ({ name, cards, setCards }) => {
  return (
    <ColumnContainer>
      <h1 className="tableName">{name}</h1>
      <CardsContainer>
        {cards
          .filter((card) => card.lista === name)
          .map((card) => (
            <Card
              key={card.id}
              id={card.id}
              titulo={card.titulo}
              conteudo={card.conteudo}
              lista={card.lista}
              cards={cards}
              setCards={setCards}
            />
          ))}
      </CardsContainer>
    </ColumnContainer>
  )
}

export default Column
