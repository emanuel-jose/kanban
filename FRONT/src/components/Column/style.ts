import styled from 'styled-components'

export const ColumnContainer = styled.div`
  background-color: var(--bg-column-color);

  width: 80vw;
  height: 65rem;

  border-radius: 5px;

  padding: 0.5rem 0;
  margin: 1rem 0;

  overflow: auto;

  h1.tableName {
    text-align: center;
    color: var(--text-column-color);
    font-size: 1.8rem;
  }

  display: flex;
  flex-direction: column;
  align-items: center;

  @media (min-width: 720px) {
    width: 30rem;
    height: 95vh;

    margin: 1rem 1rem;
  }
`
export const CardsContainer = styled.div`
  background-color: #dfdfdf;

  width: 70vw;
  min-height: max-content;

  border-radius: 5px;

  display: flex;
  flex-direction: column;
  align-items: center;

  @media (min-width: 720px) {
    width: 27rem;
  }
`
