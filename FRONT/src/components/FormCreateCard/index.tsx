import React, { useState } from 'react'
import CreateCardButton from '../CreateCardButton'
import Form from '../Form'
import Input from '../Input'
import TextArea from '../TextArea'
import { IoMdArrowRoundDown, IoMdAdd } from 'react-icons/io'
import { MainContainer, FormContainer } from './style'
import axios from 'axios'

import { CardSchema } from '../../interfaces/card'

type FieldsValues = Omit<CardSchema, 'lista' | 'id'>

interface FormCardProps {
  setCards: React.Dispatch<React.SetStateAction<CardSchema[]>>
}

const FormCreateCard: React.FC<FormCardProps> = ({ setCards }) => {
  const [openForm, setOpenForm] = useState<boolean>(false)
  const [fields, setFields] = useState<FieldsValues>({
    titulo: '',
    conteudo: ''
  })

  const handleTitle = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const { conteudo } = fields

    setFields({
      titulo: e.target.value,
      conteudo
    })
  }

  const handleContent = (e: React.ChangeEvent<HTMLTextAreaElement>): void => {
    const { titulo } = fields

    setFields({
      titulo,
      conteudo: e.target.value
    })
  }

  const handleOpenForm = (): void => {
    setOpenForm(true)
  }

  const handleCloseForm = (): void => {
    setOpenForm(false)
  }

  const populateCards = (): void => {
    axios
      .get('http://0.0.0.0:5000/cards/', {
        headers: {
          Accept: 'text/html',
          ContentType: 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then((res) => setCards(res.data))
      .catch((err) => console.log(err))
  }

  const addCardOnDB = (): void => {
    const data = {
      titulo: fields.titulo ? fields.titulo : 'Titulo',
      conteudo: fields.conteudo ? fields.conteudo : 'Description',
      lista: 'TODO'
    }

    axios
      .post('http://0.0.0.0:5000/cards/', data, {
        headers: {
          Accept: 'text/html',
          ContentType: 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(() => populateCards())
      .catch((err) => console.log(err))
  }

  const handleForm = (e: React.ChangeEvent<HTMLFormElement>): void => {
    e.preventDefault()

    if (!openForm) {
      handleOpenForm()
    }

    if (openForm) {
      addCardOnDB()
      handleCloseForm()
    }
  }

  return (
    <MainContainer>
      <FormContainer>
        <Form onSubmit={handleForm} open={openForm}>
          {openForm && (
            <>
              <Input placeholder="Title" onChange={handleTitle} />
              <TextArea placeholder="Description..." onChange={handleContent} />
            </>
          )}
          <CreateCardButton
            icons={openForm ? <IoMdAdd /> : <IoMdArrowRoundDown />}
            type="submit"
            title={openForm ? 'add card' : 'open form'}
          />
        </Form>
      </FormContainer>
    </MainContainer>
  )
}

export default FormCreateCard
