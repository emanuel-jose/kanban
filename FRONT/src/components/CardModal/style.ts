import styled from 'styled-components'

export const CardContainer = styled.div`
  background-color: var(--bg-card-color);
  width: 90vw;
  height: 35rem;
  border-radius: 5px;

  display: flex;
  flex-direction: column;
  align-items: center;

  @media (min-width: 720px) {
    width: 55rem;
    height: 45rem;
  }
`
export const BackGroundDisable = styled.div`
  position: fixed;
  top: 0;
  left: 0;

  display: flex;
  align-items: center;
  justify-content: center;

  background-color: #00000040;
  width: 100vw;
  height: 100vh;
`
export const HeaderModal = styled.div`
  width: 90%;

  display: flex;
  align-items: center;
  justify-content: space-between;

  h1 {
    color: var(--card-text-color);
  }

  @media (min-width: 720px) {
    height: 7rem;
  }
`

export const Content = styled.div`
  background-color: var(--field-text-bg-color);
  width: 75vw;
  height: 25rem;
  border-radius: 5px;

  overflow: auto;

  padding-left: 1rem;
  padding-top: 1rem;

  font-size: 1.4rem;
  color: var(--font-text-color);

  @media (min-width: 720px) {
    width: 50rem;
    height: 30rem;
  }
`
export const Footer = styled.div`
  width: 90%;

  margin-top: 1rem;
  margin-bottom: 1rem;

  display: flex;
  justify-content: space-between;
  align-items: center;

  .space {
    width: 41px;
    height: 37px;

    /* background-color: white; */
  }
`
