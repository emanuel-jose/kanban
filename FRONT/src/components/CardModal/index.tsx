import React, { useState } from 'react'
import EditModal from '../EditModal'
import DeleteNotice from '../DeleteNotice'
import IconButton from '../IconButton'
import { MdEdit } from 'react-icons/md'
import { FaTrash } from 'react-icons/fa'
import { CgCloseR } from 'react-icons/cg'
import { HiChevronLeft, HiChevronRight } from 'react-icons/hi'
import {
  CardContainer,
  BackGroundDisable,
  HeaderModal,
  Content,
  Footer
} from './style'

import { CardSchema } from '../../interfaces/card'
import axios from 'axios'
import marked from 'marked'

interface CardModelProps extends CardSchema {
  setOpenModal: React.Dispatch<React.SetStateAction<boolean>>
  setCards: React.Dispatch<React.SetStateAction<CardSchema[]>>
  cards: CardSchema[]
}

const CardModel: React.FC<CardModelProps> = ({
  id,
  titulo,
  conteudo,
  lista,
  setOpenModal,
  setCards,
  cards
}) => {
  const [openEdit, setOpenEdit] = useState<boolean>(false)
  const [deleteNotice, setDeleteNotice] = useState<boolean>(false)

  const markText = marked(conteudo)

  const handleOpenEdit = (): void => {
    setOpenEdit(true)
  }

  const handleDelete = (): void => {
    setDeleteNotice(true)
  }

  const moveToButtonLeftTitle = (): string => {
    if (lista === 'DOING') {
      return 'move to "TODO"'
    } else if (lista === 'DONE') {
      return 'move to "DOING"'
    }

    return ''
  }

  const moveToButtonRightTitle = (): string => {
    if (lista === 'TODO') {
      return 'move to "DOING"'
    } else if (lista === 'DOING') {
      return 'move to "DONE"'
    }

    return ''
  }

  const populateCards = () => {
    axios
      .get('http://0.0.0.0:5000/cards/', {
        headers: {
          Accept: 'text/html',
          ContentType: 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then((res) => setCards(res.data))
      .catch((err) => console.log(err))
  }

  const updateCardsDB = (card: CardSchema) => {
    axios
      .put(`http://0.0.0.0:5000/cards/${card.id}`, card, {
        headers: {
          Accept: 'text/html',
          ContentType: 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(() => populateCards())
      .catch((err) => console.log(err))
  }

  const moveColumn = (to: 'right' | 'left'): void => {
    const newCards = cards.map((card) => card)

    const indexCard = newCards.findIndex((card) => card.id === id)

    const updateCard = newCards[indexCard]

    if (to === 'right') {
      if (updateCard.lista === 'TODO') {
        updateCard.lista = 'DOING'
      } else if (updateCard.lista === 'DOING') {
        updateCard.lista = 'DONE'
      }
    }

    if (to === 'left') {
      if (updateCard.lista === 'DOING') {
        updateCard.lista = 'TODO'
      } else if (updateCard.lista === 'DONE') {
        updateCard.lista = 'DOING'
      }
    }

    updateCardsDB(updateCard)
  }

  return (
    <BackGroundDisable>
      {openEdit ? (
        <EditModal
          setCards={setCards}
          cards={cards}
          id={id}
          titulo={titulo}
          conteudo={conteudo}
          setOpenEdit={setOpenEdit}
        />
      ) : (
        <CardContainer>
          <HeaderModal>
            <IconButton
              icon={<MdEdit />}
              onClick={handleOpenEdit}
              title="edit card"
            />
            <h1>{titulo}</h1>
            <IconButton
              icon={<FaTrash />}
              onClick={handleDelete}
              title="delete card"
            />
          </HeaderModal>
          <Content dangerouslySetInnerHTML={{ __html: markText }}></Content>
          <Footer>
            {lista === 'DOING' || lista === 'DONE' ? (
              <IconButton
                icon={<HiChevronLeft />}
                size={25}
                title={moveToButtonLeftTitle()}
                onClick={() => moveColumn('left')}
              />
            ) : (
              <div className="space" />
            )}
            <IconButton
              icon={<CgCloseR />}
              size={25}
              onClick={() => setOpenModal(false)}
              title="close"
            />
            {lista === 'TODO' || lista === 'DOING' ? (
              <IconButton
                icon={<HiChevronRight />}
                size={25}
                title={moveToButtonRightTitle()}
                onClick={() => moveColumn('right')}
              />
            ) : (
              <div className="space" />
            )}
          </Footer>
        </CardContainer>
      )}
      {deleteNotice && (
        <DeleteNotice
          id={id}
          setCards={setCards}
          setOpenModal={setOpenModal}
          setDeleteNotice={setDeleteNotice}
        />
      )}
    </BackGroundDisable>
  )
}

export default CardModel
