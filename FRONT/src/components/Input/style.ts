import styled from 'styled-components'

export const Input = styled.input`
  width: 65vw;
  height: 4rem;
  margin: 2rem 0;
  padding-left: 1rem;

  border: none;
  border-radius: 5px;

  font-size: 1.6rem;
  color: var(--font-text-color);

  @media (min-width: 720px) {
    width: 27rem;
  }
`
