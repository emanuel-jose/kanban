import React, { InputHTMLAttributes } from 'react'
import { Input } from './style'

type InputProps = InputHTMLAttributes<HTMLInputElement>

const InputComponent: React.FC<InputProps> = ({ ...rest }) => {
  return <Input {...rest} />
}

export default InputComponent
