import { createGlobalStyle } from 'styled-components'

export const GlobalStyle = createGlobalStyle`
  :root{
    /* background screen */
    --bg-color: #C6B2F5;

    /* button create card */
    --bg-button-color: #AC95F0;
    --icons-button-color: #1A1A1A;
    --bg-btn-hover: #A990F1;
    --bg-card-icons-color: #F0F0F7;

    /* form box create card */
    --bg-form: #AC95F0;
    --field-text-bg-color: #F0F0F7;
    --font-text-color: #1A1A1A;
    --placeholder-text-field: #A1A1A1;

    --text-column-color: #9871F5;
    --bg-card-color: #9871F5;
    --bg-column-color: #F0F0F7;
    --card-text-color: #F0F0F7;

    --card-hover: #8759F4;

    font-family: 'Roboto', sans-serif;
    font-size: 10px;
  }

  body {
    margin: 0;
    padding: 0;
    background-color: var(--bg-color)
  }

`
